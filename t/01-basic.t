use v6;
use Test;
use lib 'lib';

my @modules =
	'TrimRat',
;
for @modules -> $module {
	use-ok "$module", "The $module module can be use-d.";
}

done-testing;

=finish
