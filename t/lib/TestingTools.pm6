use v6;
use fatal;
use Test;

unit module TestingTools;

# recursively search through the directory tree given in $path and return only the files whose name matches $regex
sub locate( :$path, :$regex ) is export {
	my @paths = dir( $path );
	gather while @paths.elems >= 1 {
		my $p = @paths.shift;
		take $p if $p ~~ :f and $p ~~ $regex;
		if $p ~~ :d {
			@paths.append: dir( $p );
			CATCH { when X::IO::Dir { next; } }
		}
	}
}

# read in the contents of the file given in $file and returns the stuff which matches $regex
sub search( :$file, :$regex ) is export {
	my %found;
	if $file.IO ~~ :f {
		my $data = $file.IO.slurp;
		while $data ~~ $regex {
			if %found{"$1"} !~~ Array { %found{"$1"} = []; }
			%found{"$1"}.push( "$0" );
			$data ~~ s/ [ ^ || ^^ ] .*? $regex //;
		}
	}
	my @list = %found.keys.sort;
	return @list;
}

=finish
