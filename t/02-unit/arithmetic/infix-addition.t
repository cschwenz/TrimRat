use v6;
use Test;
use lib 'lib';
use TrimRat;

my Int constant $TEST-PRECISION = 46;

my @tests =
	[ [1, 1], [1, 1], [2, 1] ],
	[ [3, 1], [4, 1], [7, 1] ],
	[ [1, 3], [1, 7], [10, 21] ],
	[ [1, 10], [2, 10], [3, 10], [3, 5], ],
	[ [1, 333], [1, 777], [10, 2331] ],
	[ [1, 333**17], [1, 777**19], [1, 333**17] ],
	[ [1, 3333**17], [1, 7777**19], [0, 1] ],
	[ [27783742160348572763840067510872319735331613, 27783742160348572763840067510872319734178289], [2002841, 2207], [1, 2], [5, 3], [2, 7], [2346040767398781173074912874653298970006137869169249, 2575386195811350603771391217852798805439922320566] ],
;

my Int $test-count = 0;
for @tests -> $test {
	$test-count++;
	cmp-ok $test.elems, '>=', 3, "Infix addition test $test-count ...";
	my $expected = $test.pop;
	my TrimRat $test-sum = TrimRat.new(0, 1).precision( $TEST-PRECISION );
	for @($test) -> $number {
		my TrimRat $n = TrimRat.new( $number.[0], $number.[1] ).precision( $TEST-PRECISION );
		$test-sum = $test-sum + $n;
	}
	cmp-ok $test-sum.numerator, '==', $expected.[0], "    ... found expected numerator.";
	cmp-ok $test-sum.denominator, '==', $expected.[1], "    ... found expected denominator.";
	"$test-sum" ~~ m/ '.' ( <[0..9]>* ) $ /;
	cmp-ok $0.chars, '<=', $TEST-PRECISION, "    ... valid precision";
}

done-testing;

=finish
