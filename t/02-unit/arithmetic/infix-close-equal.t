use v6;
use Test;
use lib 'lib';
use TrimRat;

my Int constant $TEST-PRECISION = 46;

my @tests =
	[ [1, 1], [1, 1], True ],
	[ [2, 1], [1, 1], False ],
	[ [0, 1], [1, 1], False ],
	[ [-1, 1], [1, 1], False ],
	[ [1, 1], [2, 1], False ],
	[ [1, 1], [0, 1], False ],
	[ [1, 4], [2, 8], True ],
	[ [2, 8], [1, 4], True ],
	[ [1, 4], [-2, 8], False ],
	[ [2, 8], [-1, 4], False ],
	[ [1, $UINT64_UPPER**17], [0, 1], True ],
	[ [0, 1], [1, $UINT64_UPPER**17], True ],
	[ [1, $UINT64_UPPER**17], [123, 123 * $UINT64_UPPER**17], True ],
	[ [123, 123 * $UINT64_UPPER**17], [1, $UINT64_UPPER**17], True ],
	[ [1, $UINT64_UPPER**17], [123, 123 * $UINT64_UPPER**17 - 1], True ],
	[ [123, 123 * $UINT64_UPPER**17 - 1], [1, $UINT64_UPPER**17], True ],
;

my Int $test-count = 0;
for @tests -> $test {
	$test-count++;
	cmp-ok $test.elems, '==', 3, "Infix approximately equal test $test-count ...";
	my Int $expected = $test.pop;
	my ($first, $second) = @($test);
	my TrimRat $a = TrimRat.new($first.[0], $first.[1]).precision( $TEST-PRECISION );
	my TrimRat $b = TrimRat.new($second.[0], $second.[1]).precision( ($TEST-PRECISION / 2).Int );
	my Int $result = $a ≅ $b;
	cmp-ok $result, '==', $expected, "    ... found expected result."
		or diag "$first.[0]/$first.[1] <=> $second.[0]/$second.[1] returned $result instead of $expected";
}

done-testing;

=finish
