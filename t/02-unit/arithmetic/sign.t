use v6;
use Test;
use lib 'lib';
use TrimRat;

my Int constant $TEST-PRECISION = 46;

my @tests =
	[ [1, 1], [1, 1] ],
	[ [3, 1], [1, 1] ],
	[ [-1, 1], [-1, 1] ],
	[ [-3, 1], [-1, 1] ],
	[ [1, 10**45], [1, 1] ],
	[ [3, 10**45], [1, 1] ],
	[ [-1, 10**45], [-1, 1] ],
	[ [-3, 10**45], [-1, 1] ],
	[ [0, 1], [0, 1] ],
	[ [0, 10**45], [0, 1] ],
;

my Int $test-count = 0;
for @tests -> $test {
	$test-count++;
	my $expected = $test.pop;
	my ($numerator, $denominator) = @($test.[0]);
	my TrimRat $expected-sign = TrimRat.new($expected.[0], $expected.[1]).precision($TEST-PRECISION);
	my TrimRat $test-sign = TrimRat.new($numerator, $denominator).precision($TEST-PRECISION).sign;
	cmp-ok $test-sign, '==', $expected-sign, ".sign() test $test-count ...";
	"$test-sign" ~~ m/ '.' ( <[0..9]>* ) $ /;
	cmp-ok $0.chars, '<=', $TEST-PRECISION, "    ... valid precision";
}

done-testing;

=finish
