use v6;
use Test;
use lib 'lib';
use TrimRat;

my Int constant $DEFAULT-MULTIPLIER = 3;
my Int constant $DEFAULT-PRECISION = do { my Int $n = 1; $n++ while ( 1 / 10**$n ).^name eq "Rat"; $n * $DEFAULT-MULTIPLIER };
my Int constant $TEST-PRECISION = 36;

my @tests =
	[ 127, 239, Nil, [127, 239], '0.531380753138075313807531380753138075313807531380753138075314', 0, [127, 239], [127, 239], [127,239] ],
	[ 127, 239, $TEST-PRECISION, [127, 239], '0.531380753138075313807531380753138075', 0, [127, 239], [127, 239], [127, 239] ],
	[ 127, $UINT64_UPPER, $TEST-PRECISION, [127, $UINT64_UPPER], '0.000000000000000006884683795282953156', 0, [127, $UINT64_UPPER], [127, $UINT64_UPPER], [127, $UINT64_UPPER] ],
	[ 65537, $UINT64_UPPER + 1, $TEST-PRECISION, [65537, $UINT64_UPPER + 1], '0.000000000000003552767888909125204385', 0, [65537, $UINT64_UPPER], [65537, $UINT64_UPPER], [65537, $UINT64_UPPER + 1] ],
	[ $UINT64_UPPER**4, 3 * $UINT64_UPPER**4 + 1, $TEST-PRECISION, [1, 3], '0.333333333333333333333333333333333333', 0, [1, 3], [1, 3], [1, 3] ],
;

my Int $test-count = 0;
for @tests -> $test {
	$test-count++;
	my ( $numerator, $denominator, $precision, $num-den, $string, $integer, $rat, $fatrat, $trimrat ) = @($test);
	my $obj = $precision ~~ Int
		?? TrimRat.new( $numerator, $denominator ).precision( $precision )
		!! TrimRat.new( $numerator, $denominator ) ;
	isa-ok $obj, 'TrimRat', "[Test $test-count] .new() ...";
	cmp-ok $obj.precision, '==', ($precision // $DEFAULT-PRECISION), "    ... has correct .precision()";
	cmp-ok $obj.numerator, '==', $num-den.[0], "    ... has correct .numerator()";
	cmp-ok $obj.denominator, '==', $num-den.[1], "    ... has correct .denominator()";
	cmp-ok $obj.name, 'eq', 'TrimRat', "    ... has correct .name()";
	cmp-ok $obj.perl, 'eq', "TrimRat.new($num-den.[0], $num-den.[1]).precision(" ~ ($precision // $DEFAULT-PRECISION) ~ ")", "    ... has correct .perl()";
	cmp-ok $obj.gist, 'eq', $string, "    ... has correct .gist()";
	cmp-ok $obj.Str, 'eq', $string, "    ... has correct .Str()";
	cmp-ok $obj.Int, '==', $integer, "    ... has correct .Int()";
	my Rat $obj-rat = $obj.Rat;
	cmp-ok $obj-rat.numerator, '==', $rat.[0], "    ...  has correct .Rat() numerator";
	cmp-ok $obj-rat.denominator, '==', $rat.[1], "    ... has correct .Rat() denominator";
	my FatRat $obj-fatrat = $obj.FatRat;
	cmp-ok $obj-rat.numerator, '==', $fatrat.[0], "    ... has correct .FatRat() numerator";
	cmp-ok $obj-rat.denominator, '==', $fatrat.[1], "    ... has correct .FatRat() denominator";
	my TrimRat $obj-trimrat = $obj.TrimRat;
	cmp-ok $obj-trimrat.numerator, '==', $trimrat.[0], "    ... has correct .TrimRat() numerator";
	cmp-ok $obj-trimrat.denominator, '==', $trimrat.[1], "    ... has correct .TrimRat() denominator";
	cmp-ok $obj-trimrat.precision, '==', ($precision // $DEFAULT-PRECISION), "    ... has correct .TrimRat() precision";
	cmp-ok $obj.Num, '==', ($numerator / $denominator).Num, "    ... has correct .Num()";
	cmp-ok $obj.Bool, '==', True, "    ... has correct .Bool()";
}

done-testing;

=finish
