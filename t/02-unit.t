use v6;
use Test;
use lib 't/lib';
use TestingTools;

my IO::Path $path = "$?FILE".IO;
my Str $dist-dir = ( $path.volume.Str ~ $path.dirname.IO.parent.Str ).IO.resolve.Str;
my Str $lib-dir = ( $path.volume.Str ~ $path.dirname.IO.parent.child( 'lib' ).Str ).IO.resolve.Str;
my Str $unit-test-dir = ( $path.volume.Str ~ $path.dirname.IO.child( '02-unit' ).Str ).IO.resolve.Str;

my @modules = locate( path => $lib-dir, regex => / '.' [ p6 || pl6 || pm6 || pl || pm ] \s* $ / );
my @unit-tests = locate( path => $unit-test-dir, regex => / '.' [ t6 || t ] \s* $ / );

my %override =
	'precision' => 't/02-unit/trimrat.t',
	'numerator' => 't/02-unit/trimrat.t',
	'denominator' => 't/02-unit/trimrat.t',
	'name' => 't/02-unit/trimrat.t',
	'perl' => 't/02-unit/trimrat.t',
	'gist' => 't/02-unit/trimrat.t',
	'Str' => 't/02-unit/trimrat.t',
	'Int' => 't/02-unit/trimrat.t',
	'Rat' => 't/02-unit/trimrat.t',
	'FatRat' => 't/02-unit/trimrat.t',
	'TrimRat' => 't/02-unit/trimrat.t',
	'Num' => 't/02-unit/trimrat.t',
	'Bool' => 't/02-unit/trimrat.t',
	'infix:« + »' => 't/02-unit/arithmetic/infix-addition.t',
	'infix:« ≥ »' => 't/02-unit/arithmetic/infix-greater-or-equal.t',
	'infix:« >= »' => 't/02-unit/arithmetic/infix-greater-or-equal.t',
	'infix:« ≤ »' => 't/02-unit/arithmetic/infix-less-or-equal.t',
	'infix:« <= »' => 't/02-unit/arithmetic/infix-less-or-equal.t',
	'infix:« ≠ »' => 't/02-unit/arithmetic/infix-not-equal.t',
	'infix:« != »' => 't/02-unit/arithmetic/infix-not-equal.t',
	'infix:« < »' => 't/02-unit/arithmetic/infix-less.t',
	'infix:« > »' => 't/02-unit/arithmetic/infix-greater.t',
	'infix:« ≅ »' => 't/02-unit/arithmetic/infix-close-equal.t',
	'infix:« == »' => 't/02-unit/arithmetic/infix-equal.t',
	'infix:« <=> »' => 't/02-unit/arithmetic/infix-compare.t',
;

for @modules -> $module {
	my $name = "$module".IO.relative( $lib-dir ).Str;
	my Str $expected = "$unit-test-dir".IO.child( "$module".IO.relative( $lib-dir ).Str.lc ).Str;
	$expected ~~ s/ .* ( [ <[\/]> t <[\/]> || <[\\]> t <[\\]> ] .+? ) '.' <-[\.]>+? \h* $ /$0/;
	$expected ~~ s/ [^ || ^^ ] <[\/\\]> //;
	my @found = @unit-tests.grep({ m/ << $expected '.' [ t6 || t ] \s* $ / });
	( cmp-ok @found.elems, '==', 1, "The $name file has a matching unit-test file." )
		or ( diag "Cannot locate test file for: $expected" );
	my @ops = search( file => "$module", regex => rx/ << [ multi \h+ ]? ( [ sub || method ] ) \h+ ( [ \w+ \V*? ] ) \h* <[\(]> <-[\(\)\{\}]>* <[\)]> <-[\(\)\{\}]>* <[\{]> / );
	for @ops -> $op {
		my @found = @unit-tests.grep({ m/ << [ $op ] '.' [ t6 || t ] \s* $ / }).map({ "$_"; });
		if @found.elems < 1 and ( defined %override{"$op"} and %override{"$op"}.IO.e and %override{"$op"}.IO.f ) { @found.push( %override{"$op"} ) }
		( cmp-ok @found.elems, '>=', 1, "    ... '$op' has its own unit-test file." )
			or ( diag "Cannot find the unit-test file for: $op" );
	}
}

done-testing;

=finish
