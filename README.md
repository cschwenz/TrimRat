NAME
====

TrimRat - Arbitrary precision math for Rakudo Perl 6.


SYNOPSIS
========

    use TrimRat;
    my TrimRat $tr = TrimRat.new(17, 19).precision(30);


DESCRIPTION
===========

If you need to specify your precision, then you want to use TrimRat.


INSTALL
=======

To install with `zef`:

    zef install TrimRat

To install a locally downloaded copy with `zef`:

    unzip ./TrimRat-master-*.{zip,tar.gz,tar.bz2,tar}
    prove --exec perl6 -r --verbose ./TrimRat-master-*/t/
    zef --dry install ./TrimRat-master
    zef install ./TrimRat-master


AUTHOR
======

Calvin Schwenzfeier <calvin.schwenzfeier@gmail.com>


COPYRIGHT AND LICENSE
=====================

Copyright 2017 Calvin Schwenzfeier

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.
