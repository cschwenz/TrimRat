use v6;
use fatal;
use nqp;
use MONKEY-TYPING;

my Int constant $SCORCHED-EARTH = 7;
my Int constant $DEFAULT-MULTIPLIER = 3;
my UInt constant $DEFAULT-PRECISION = do { my Int $n = 1; $n++ while ( 1 / 10**$n ).^name eq "Rat"; ($n * $DEFAULT-MULTIPLIER).abs.UInt };  # the default display precision
my FatRat constant $DEFAULT-EPSILON = FatRat.new(1, 10**( ( $DEFAULT-PRECISION / $DEFAULT-MULTIPLIER ).Int - 1 ));  # for converting TrimRat's to Rat and Num's to TrimRat

enum RoundingMode <TRADITIONAL EVEN ODD ALTERNATE RANDOM>;

class TrimRat is Cool does Rational[Int, Int] {

	has UInt $!precision = $DEFAULT-PRECISION;
	has RoundingMode $!rounding-mode;

	multi method precision() returns Int {
		if not $!precision { $!precision //= $DEFAULT-PRECISION; }
		return $!precision.Int;
	}
	multi method precision( UInt $pr ) returns TrimRat {
		$!precision = $pr;
		self.norm;
		return self;
	}
	method !scorched-earth() returns Int { return self.precision() + $SCORCHED-EARTH; }
	multi method rounding-mode() returns RoundingMode {
		my RoundingMode $mode = $!rounding-mode;
		return $mode;
	}
	multi method rounding-mode( RoundingMode $mode ) returns TrimRat {
		$!rounding-mode = $mode;
		self.norm;
		return self;
	}

	method !reduce() {
		if $!denominator > 1 {
			my Int $gcd = $!denominator gcd $!numerator;
			if $gcd > 1 {
				nqp::bindattr( self, self.WHAT, '$!numerator', $!numerator div $gcd );
				nqp::bindattr( self, self.WHAT, '$!denominator', $!denominator div $gcd );
			}
		}
		return;
	}
	method !round-internal( FatRat $number ) returns FatRat {
		my FatRat $num = $number.clone;
		if $num.denominator > 1 {
			my Int $gcd = $num.denominator gcd $num.numerator;
			if $gcd > 1 { $num = FatRat.new( $num.numerator div $gcd, $num.denominator div $gcd ); }
		}
		if $num.denominator > 10**(self!scorched-earth) {
			my FatRat $epsilon = FatRat.new( 1, 10**(self!scorched-earth + 1) );
			my FatRat $half = FatRat.new(1, 2);
			if $num > 0 { $num = $epsilon * ( ($num + $half * $epsilon) / $epsilon ).floor.FatRat; }
			elsif $num < 0 { $num = $epsilon * ( ($num + $half * $epsilon) / $epsilon ).ceiling.FatRat; }
			else { $num = FatRat.new(0, 1); }
			if $num.denominator > 1 {
				my Int $gcd = $num.denominator gcd $num.numerator;
				if $gcd > 1 { $num = FatRat.new( $num.numerator div $gcd, $num.denominator div $gcd ); }
			}
		}
		return $num;
	}
	method !factorial( FatRat $num ) returns FatRat {
		my FatRat $prod = FatRat.new(1, 1);
		my Int $max = $num.Int;
		loop ( my Int $x = 2 ; $x <= $max ; $x++ ) { $prod *= FatRat.new($x, 1); }
		return $prod;
	}
	method !ln( FatRat $num ) returns FatRat {
		my FatRat $answer;
		# ln(z) = 2 ∑ n=0→∞ [ 1/(2n+1) ( (z-1)/(z+1) )^(2n+1) ]
		if $num > 0 {
			my FatRat $one = FatRat.new(1, 1);
			my FatRat $two = FatRat.new(2, 1);
			my Int $odd = 1;
			my FatRat $ratio = ($num - $one) / ($num + $one); # this is intentionally untruncated.
			my FatRat $prev = $ratio.clone;
			$answer = self!round-internal( $prev + FatRat.new(1, $odd) * self!exponent-n($ratio, $odd) );
			my FatRat $delta = ( ($two * $answer) - ($two * $prev) ).abs;
			my FatRat $err = FatRat.new( 1, 10**(self!scorched-earth) );
			while $delta > $err {
				$prev = $answer.clone;
				$odd += 2;
				$answer = self!round-internal( $prev + FatRat.new(1, $odd) * self!exponent-n($ratio, $odd) );
				$delta = ( ($two * $answer) - ($two * $prev) ).abs;
			}
			$answer = self!chop-precision( $two * $answer );
		}
		return $answer;
	}
	method !exponent-n( FatRat $number, Int $pr ) {
		my Int $power = $pr;
		my Bool $is-negative = False;
		if $power < 0 {
			$is-negative = True;
			$power = $power.abs;
		}
		if $power == 0 {
			return FatRat.new(1, 1);
		}
		elsif $power == 1 {
			my FatRat $pow = self!round-internal( $number );
			if $is-negative { return FatRat.new($pow.denominator, $pow.numerator); }
			return $pow;
		}
		elsif $power == 2 {
			my FatRat $pow = self!round-internal( $number * $number );
			if $is-negative { return FatRat.new($pow.denominator, $pow.numerator); }
			return $pow;
		}
		elsif $power == 3 {
			my FatRat $pow = self!round-internal( $number * $number * $number );
			if $is-negative { return FatRat.new($pow.denominator, $pow.numerator); }
			return $pow;
		}
		my %ptable = '0' => FatRat.new(1, 1), '1' => $number.FatRat;
		{
			my Int $pow = 2;
			my FatRat $result = %ptable{'1'}.clone;
			while $pow <= $power {
				%ptable{"$pow"} = $result * $result;
				$result = self!round-internal( %ptable{"$pow"} );
				$pow *= 2;
			}
		};
		my Int @pbit = $power.polymod(2 xx *).list;
		my FatRat $num = %ptable{'0'}.clone;
		loop ( my $x = 0 ; $x < @pbit.elems ; $x++ ) {
			next if @pbit[$x] != 1;
			$num = self!round-internal( $num * %ptable{ "" ~ 2**$x } );
		}
		if $is-negative { $num = FatRat.new($num.denominator, $num.numerator); }
		return $num;
	}
	method !root-n( FatRat $number, Int $root ) returns FatRat {
		my Int $rt = $root;
		my Bool $is-negative = False;
		if $rt < 0 {
			$is-negative = True;
			$rt = $rt.abs;
		}
		if $number < 0 and $rt %% 2 {
			# TODO: make a ComplexTrimRat data type.
			X::NYI.new(feature => "ComplexTrimRat data-type").throw;
			return;
		}
		elsif $number == -1 and $rt %% 2 {
			# TODO: make a ComplexTrimRat data type.
			X::NYI.new(feature => "ComplexTrimRat data-type").throw;
			return;
		}
		elsif $number == 0 or $number == 1 or ($number == -1 and not $rt %% 2) {
			return $number.clone;
		}
		my FatRat $c = self!solve-bisect( fn => sub ( FatRat $n ) { return self!exponent-n($n, $rt) - $number; }, min => FatRat.new(0, 1), max => $number );
		if $is-negative {
			$c = FatRat.new($c.denominator, $c.numerator);
		}
		return $c;
	}
	# $fn  the function where we want to find the zero, must return a FatRat
	# $min  the lower bound of the range to search, typically FatRat.new(0, 1)
	# $max  the upper bound of the range to search, typically the number you're trying to find a solution for
	method !solve-bisect( Sub :$fn, FatRat :$min, FatRat :$max ) returns FatRat {
		my FatRat $c;
		{
			my FatRat $err = FatRat.new( 1, 10**(self!scorched-earth) );
			my FatRat $two = FatRat.new(2, 1);
			my FatRat $a = $min.clone;
			my FatRat $b = $max.clone;
			my FatRat $fn-a = $fn.($a);
			my FatRat $fn-b = $fn.($b);
			my FatRat $delta = ($a - $b).abs;
			while $delta > $err {
				$c = self!chop-precision( ($a + $b) / $two );
				my FatRat $fn-c = $fn.($c);
				if $fn-c == 0 { return $c; }
				if $fn-a.sign.Int == $fn-c.sign.Int {
					$a = $c.clone;
					$fn-a = $fn-c.clone;
				}
				else {
					$b = $c.clone;
					$fn-b = $fn-c.clone;
				}
				$delta = ($a - $b).abs;
			}
			$c = self!round-internal( ($a + $b) / $two );
		};
		return $c;
	}

	method name() returns Str { return "TrimRat"; }
	method perl() returns Str {
		self.norm;
		return "TrimRat.new($!numerator, $!denominator).precision(" ~ self.precision ~ ")";
	}
	multi method gist() returns Str {
		my FatRat $pr = FatRat.new( 1, 10**self.precision );
		my TrimRat $gist = self.norm.clone.round( FatRat.new( 1, 10**self.precision ) );
		my Int $whole-part = $gist.clone.abs.floor.Int;
		my Int $fractional-part = ( ($gist.clone.abs - $whole-part) * 10**self.precision ).floor.Int;
		$whole-part *= self.sign.Int;
		return "$whole-part" ~ ( '.' ~ ('0' x (self.precision - $fractional-part.chars)) ~ $fractional-part );
	}
	multi method Str() is default returns Str { return self.gist; }
	method Int() returns Int { return self.FatRat.Int; }
	method Rat() returns Rat {
		my TrimRat $num = self.clone;
		if $num.denominator >= $UINT64_UPPER { $num = $num.round( FatRat.new(1, $UINT64_UPPER) ); }
		return Rat.new( $num.numerator, $num.denominator );
	}
	method FatRat() returns FatRat { return FatRat.new($!numerator, $!denominator); }
	method TrimRat() returns TrimRat { return self.clone; }
	method Num() returns Num { return self.FatRat.Num; }
	multi method Bool() returns Bool { return $!numerator != 0 and $!denominator != 0; }

	method norm() returns TrimRat {
		if $!denominator < 0 {
			nqp::bindattr( self, self.WHAT, '$!numerator', -$!numerator );
			nqp::bindattr( self, self.WHAT, '$!denominator', -$!denominator );
		}
		self!reduce;
		if $!denominator > 10**self!scorched-earth {
			nqp::bindattr( self, self.WHAT, '$!numerator', ( self.FatRat * 10**self!scorched-earth ).Int );
			nqp::bindattr( self, self.WHAT, '$!denominator', 10**self!scorched-earth );
			self!reduce;
		}
		return self;
	}
	method sign() returns TrimRat {
		my TrimRat $sign;
		if self > 0 { $sign = TrimRat.new(1, 1).precision( self.precision ); }
		elsif self < 0 { $sign = TrimRat.new(-1, 1).precision( self.precision ); }
		else { $sign = TrimRat.new(0, 1).precision( self.precision ); }
		return $sign;
	}

	method floor() returns TrimRat {
		my TrimRat $floor = TrimRat.new( self.norm.FatRat.floor, 1 ).precision( self.precision );
		return $floor;
	}
	multi sub floor( TrimRat $number ) returns TrimRat is export { return $number.clone.floor; }

	method ceiling() returns TrimRat {
		my TrimRat $ceiling = TrimRat.new( self.norm.FatRat.ceiling, 1 ).precision( self.precision );
		return $ceiling;
	}
	multi sub ceiling( TrimRat $number ) returns TrimRat is export { return $number.clone.ceiling; }

	multi method round( Rational $epsilon = 1/1 ) returns TrimRat {  # rounds to nearest $epsilon
		self.norm;
		my FatRat $eps = FatRat.new( $epsilon.numerator, $epsilon.denominator );
		my TrimRat $round = self.clone;
		if self > 0 {
			my FatRat $numerator = FatRat.new($!numerator, $!denominator) + FatRat.new(1, 2) * $eps;
			my FatRat $n = $numerator / $eps;
			my FatRat $num = $eps * $n.floor.FatRat;
			$round = TrimRat.new( $num.numerator, $num.denominator ).precision( self.precision );
		}
		elsif self < 0 {
			my FatRat $numerator = FatRat.new($!numerator, $!denominator) - FatRat.new(1, 2) * $eps;
			my FatRat $n = $numerator / $eps;
			my FatRat $num = $eps * $n.ceiling.FatRat;
			$round = TrimRat.new( $num.numerator, $num.denominator ).precision( self.precision );
		}
		else {
			$round = TrimRat.new( 0, 1 ).precision( self.precision );
		}
		return $round;
	}
	multi sub round( TrimRat $number ) returns TrimRat is export { return $number.clone.round; }  # rounds $number to nearest integer

	method truncate() returns TrimRat {
		self.norm;
		if self > 0 { self.floor; }
		elsif self < 0 { self.ceiling; }
		else {
			$!numerator = 0;
			$!denominator = 1;
		}
		return self;
	}

	method abs() returns TrimRat {
		$!numerator = $!numerator.abs;
		$!denominator = $!denominator.abs;
		return self;
	}
	multi sub abs( TrimRat $number ) returns TrimRat is export { return $number.clone.abs; }

	method sqrt() returns TrimRat {
		if self < 0 {
			# TODO: make a ComplexTrimRat data type.
			X::NYI.new(feature => "ComplexTrimRat data-type").throw;
			$!numerator = 0;
			$!denominator = 0;  # "I say we take off and nuke the entire site from orbit. It's the only way to be sure."
			return self;
		}
		elsif self == 0 {
			$!numerator = 0;
			$!denominator = 1;
			return self;
		}
		elsif self == 1 {
			$!numerator = 1;
			$!denominator = 1;
			return self;
		}
		my FatRat $x = self!round-internal( self!root-n( self.FatRat, 2 ) );
		$!numerator = $x.numerator;
		$!denominator = $x.denominator;
		return self;
	}
	multi sub sqrt( TrimRat $number ) returns TrimRat is export { return $number.clone.sqrt; }

	sub srand( TrimRat $number ) returns TrimRat is export {
		!!!;
	}
# sub srand(Int $seed --> Int:D)

	method rand() returns TrimRat { # Returns a pseudo-random number between zero and the number.
		!!!;
	}
	multi sub term:<rand>() returns TrimRat is export { # The term form returns a pseudo-random Num between 0e0 and 1e0.
		!!!;
	}
# method rand()
# sub term:<rand> (--> Num:D)
# method rand(Real:D: --> Real:D)
# method rand(Num:D: --> Num)

	multi method exp( TrimRat $base = self.e ) returns TrimRat {
		my FatRat $power = self.norm.FatRat;
		my Bool $is-negative = False;
		if $power < 0 {
			$is-negative = True;
			$power = $power.abs;
		}
		my FatRat $err = FatRat.new( 1, 10**(self!scorched-earth) );
		my FatRat $one = FatRat.new(1, 1);
		my FatRat $two = FatRat.new(2, 1);
		my FatRat $low-x = FatRat.new($power.floor.Int, 1);
		my FatRat $high-x = FatRat.new($power.ceiling.Int, 1);
		my FatRat $low = self!exponent-n( $base.FatRat, $power.floor.Int );
		my FatRat $high = self!exponent-n( $base.FatRat, $power.ceiling.Int );
		my FatRat $approx = ($low-x + $high-x) / $two;
		my FatRat $delta = ($power - $approx).abs;
		my Int $rt = 2;
		my FatRat $root = self!root-n( $base.FatRat, $rt );
		my FatRat $mid = ($low * $root + $high) / ($one + $root);
		if $approx > $power {
			$high = $mid.clone;
			$high-x = $approx;
		}
		elsif $approx < $power {
			$low = $mid.clone;
			$low-x = $approx;
		}
		while $delta > $err {
			$approx = ($low-x + $high-x) / $two;
			$delta = ($power - $approx).abs;
			$rt *= 2;
			$root = self!root-n( $base.FatRat, $rt );
			$mid = ($low * $root + $high) / ($one + $root);
			if $approx > $power {
				$high = $mid.clone;
				$high-x = $approx;
			}
			elsif $approx < $power {
				$low = $mid.clone;
				$low-x = $approx;
			}
		}
		if $is-negative {
			$!numerator = $mid.denominator;
			$!denominator = $mid.numerator;
		}
		else {
			$!numerator = $mid.numerator;
			$!denominator = $mid.denominator;
		}
		self.norm;
		return self;
	}
	multi sub exp( TrimRat $power, TrimRat $base = $power.e ) returns TrimRat is export { return $power.clone.exp($base); }

	multi method log( TrimRat $base = self.e ) returns TrimRat {
		# log_b(z) = ln(z) / ln(b)
		my FatRat $ln-num = self!ln( FatRat.new(self.numerator, self.denominator) );
		my FatRat $ln-base = self!ln( FatRat.new($base.numerator, $base.denominator) );
		my FatRat $lg = $ln-num / $ln-base;
		$!numerator = $lg.numerator;
		$!denominator = $lg.denominator;
		self.norm;
		return self;
	}
	multi sub log( TrimRat $number, TrimRat $base = $number.e ) returns TrimRat is export { return $number.clone.log( $base ); }

	method log10() returns TrimRat {
		# log_b(z) = ln(z) / ln(b)
		my FatRat $ln-num = self!ln( FatRat.new(self.numerator, self.denominator) );
		my FatRat $ln-base = self!ln( FatRat.new(10, 1) );
		my FatRat $lg = $ln-num / $ln-base;
		$!numerator = $lg.numerator;
		$!denominator = $lg.denominator;
		self.norm;
		return self;
	}
	multi sub log10( TrimRat $number ) returns TrimRat is export { return $number.clone.log10; }

	method sin() returns TrimRat {
		!!!;
	}
	multi sub sin( TrimRat $number ) returns TrimRat is export { return $number.clone.sin; }
# sub sin(Numeric(Cool))
# method sin()

	method asin() returns TrimRat {
		!!!;
	}
	multi sub asin( TrimRat $number ) returns TrimRat is export { return $number.clone.asin; }
# sub asin(Numeric(Cool))
# method asin()

	method cos() returns TrimRat {
		!!!;
	}
	multi sub cos( TrimRat $number ) returns TrimRat is export { return $number.clone.cos; }
# sub cos(Numeric(Cool))
# method cos()

	method acos() returns TrimRat {
		!!!;
	}
	multi sub acos( TrimRat $number ) returns TrimRat is export { return $number.clone.acos; }
# sub acos(Numeric(Cool))
# method acos()

	method tan() returns TrimRat {
		!!!;
	}
	multi sub tan( TrimRat $number ) returns TrimRat is export { return $number.clone.tan; }
# sub tan(Numeric(Cool))
# method tan()

	method atan() returns TrimRat {
		!!!;
	}
	multi sub atan( TrimRat $number ) returns TrimRat is export { return $number.clone.atan; }
# sub atan(Numeric(Cool))
# method atan()

	multi method atan2( TrimRat $y = TrimRat.new(1, 1).precision(self.precision) ) returns TrimRat {
		!!!;
	}
	multi sub atan2( TrimRat $x, TrimRat $y = TrimRat.new(1, 1).precision($x.precision) ) returns TrimRat is export { return $x.clone.atan2($y); }
# sub atan2(Numeric() $x, Numeric() $y = 1e0)
# method atan2($y = 1e0)

	method sec() returns TrimRat {
		!!!;
	}
	multi sub sec( TrimRat $number ) returns TrimRat is export { return $number.clone.sec; }
# sub sec(Numeric(Cool))
# method sec()

	method asec() returns TrimRat {
		!!!;
	}
	multi sub asec( TrimRat $number ) returns TrimRat is export { return $number.clone.asec; }
# sub asec(Numeric(Cool))
# method asec()

	method cosec() returns TrimRat {
		!!!;
	}
	multi sub cosec( TrimRat $number ) returns TrimRat is export { return $number.clone.cosec; }
# sub cosec(Numeric(Cool))
# method cosec()

	method acosec() returns TrimRat {
		!!!;
	}
	multi sub acosec( TrimRat $number ) returns TrimRat is export { return $number.clone.acosec; }
# sub acosec(Numeric(Cool))
# method acosec()

	method cotan() returns TrimRat {
		!!!;
	}
	multi sub cotan( TrimRat $number ) returns TrimRat is export { return $number.clone.cotan; }
# sub cotan(Numeric(Cool))
# method cotan()

	method acotan() returns TrimRat {
		!!!;
	}
	multi sub acotan( TrimRat $number ) returns TrimRat is export { return $number.clone.acotan; }
# sub acotan(Numeric(Cool))
# method acotan()

	method sinh() returns TrimRat {
		!!!;
	}
	multi sub sinh( TrimRat $number ) returns TrimRat is export { return $number.clone.sinh; }
# sub sinh(Numeric(Cool))
# method sinh()

	method asinh() returns TrimRat {
		!!!;
	}
	multi sub asinh( TrimRat $number ) returns TrimRat is export { return $number.clone.asinh; }
# sub asinh(Numeric(Cool))
# method asinh()

	method cosh() returns TrimRat {
		!!!;
	}
	multi sub cosh( TrimRat $number ) returns TrimRat is export { return $number.clone.cosh; }
# sub cosh(Numeric(Cool))
# method cosh()

	method acosh() returns TrimRat {
		!!!;
	}
	multi sub acosh( TrimRat $number ) returns TrimRat is export { return $number.clone.acosh; }
# sub acosh(Numeric(Cool))
# method acosh()

	method tanh() returns TrimRat {
		!!!;
	}
	multi sub tanh( TrimRat $number ) returns TrimRat is export { return $number.clone.tanh; }
# sub tanh(Numeric(Cool))
# method tanh()

	method atanh() returns TrimRat {
		!!!;
	}
	multi sub atanh( TrimRat $number ) returns TrimRat is export { return $number.clone.atanh; }
# sub atanh(Numeric(Cool))
# method atanh()

	method sech() returns TrimRat {
		!!!;
	}
	multi sub sech( TrimRat $number ) returns TrimRat is export { return $number.clone.sech; }
# sub sech(Numeric(Cool))
# method sech()

	method asech() returns TrimRat {
		!!!;
	}
	multi sub asech( TrimRat $number ) returns TrimRat is export { return $number.clone.asech; }
# sub asech(Numeric(Cool))
# method asech()

	method cosech() returns TrimRat {
		!!!;
	}
	multi sub cosech( TrimRat $number ) returns TrimRat is export { return $number.clone.cosech; }
# sub cosech(Numeric(Cool))
# method cosech()

	method acosech() returns TrimRat {
		!!!;
	}
	multi sub acosech( TrimRat $number ) returns TrimRat is export { return $number.clone.acosech; }
# sub acosech(Numeric(Cool))
# method acosech()

	method cotanh() returns TrimRat {
		!!!;
	}
	multi sub cotanh( TrimRat $number ) returns TrimRat is export { return $number.clone.cotanh; }
# sub cotanh(Numeric(Cool))
# method cotanh()

	method acotanh() returns TrimRat {
		!!!;
	}
	multi sub acotanh( TrimRat $number ) returns TrimRat is export { return $number.clone.acotanh; }
# sub acotanh(Numeric(Cool))
# method acotanh()

	method cis() { # returns ComplexTrimRat
		# TODO: make a ComplexTrimRat data type.
		X::NYI.new(feature => "ComplexTrimRat data type").throw;
	}
	multi sub cis( TrimRat $number ) returns TrimRat is export { return $number.clone.cis; }

	method e() returns TrimRat {
		# TODO: https://en.wikipedia.org/wiki/E_(mathematical_constant)#Alternative_characterizations
		#       http://www2.stetson.edu/~efriedma/mathmagic/0804.html
		#       https://en.wikipedia.org/wiki/List_of_representations_of_e
		# e ≈ ( 1 + 9**( -4**( 7 * 6 ) ) )**( 3**( 2**85 ) )
		# e = 3 + ∑ k=2→∞ [ -1 / ( k! * (k - 1) * k ) ]   <--- implemented this one
		my FatRat $one = FatRat.new(1, 1);
		my FatRat $neg-one = FatRat.new(-1, 1);
		my FatRat $three = FatRat.new(3, 1);
		my FatRat $prev = FatRat.new(-1, 4);
		my FatRat $k = $three.clone;
		my FatRat $sum = self!internal-precision( $prev + $neg-one / ( self!factorial($k) * ( $k * $k - $k ) ) );
		my FatRat $err = FatRat.new( 1, 10**(self!scorched-earth) );
		while ( ($three + $prev) - ($three + $sum) ).abs > $err {
			$sum = self!internal-precision( $prev + $neg-one / ( self!factorial($k) * ( $k * $k - $k ) ) );
			$prev = $sum.clone;
			$k++;
		}
		$sum = $sum + $three;
		my TrimRat $trim-e = TrimRat.new( $sum.numerator, $sum.denominator ).precision( self.precision );
		return $trim-e;
	}
	multi sub term:<e>() returns TrimRat is export { return TrimRat.new(1, 1).e; }
	multi sub term:<𝑒>() returns TrimRat is export { return TrimRat.new(1, 1).e; }

	method pi() returns TrimRat {
		# https://en.wikipedia.org/wiki/Chudnovsky_algorithm
		my FatRat $sixteen = FatRat.new(16, 1);
		my FatRat $L-step = FatRat.new(545140134, 1);
		my FatRat $X-step = FatRat.new(-262537412640768000, 1);
		my FatRat $K-step = FatRat.new(12, 1);
		my FatRat $K = FatRat.new(6, 1);
		my FatRat $M = FatRat.new(1, 1);
		my FatRat $L = FatRat.new(13591409, 1);
		my FatRat $X = FatRat.new(1, 1);
		my FatRat $S = FatRat.new(13591409, 1);
		my FatRat $numerator = FatRat.new(426880, 1) * TrimRat.new(10005, 1).precision(self.precision).sqrt.FatRat;
		my FatRat $pi = self!chop-precision( $numerator / $S );
		my FatRat $delta = ( FatRat.new(3, 1) - $pi ).abs;
		my FatRat $err = FatRat.new( 1, 10**(self!scorched-earth) );
		my FatRat $n = FatRat.new(1, 1);
		while $delta >= $err {
			$M = self!internal-precision( ( ( $K * $K * $K - $K * $sixteen ) * $M ) / ( $n * $n * $n ) );
			$L += $L-step;
			$X *= $X-step;
			$S = self!internal-precision( $S + ( $M * $L ) / $X );
			$K += $K-step;
			my FatRat $approx = self!internal-precision( $numerator / $S );
			$delta = ( $pi - $approx ).abs;
			$pi = $approx.clone;
			$n++;
		}
		my TrimRat $trim-pi = TrimRat.new( $pi.numerator, $pi.denominator ).precision( self.precision );
		return $trim-pi;
	}
	multi sub term:<pi>() returns TrimRat is export { return TrimRat.new(1, 1).pi; }
	multi sub term:<π>() returns TrimRat is export { return TrimRat.new(1, 1).pi; }

	method tau() returns TrimRat {
		my TrimRat $tau = TrimRat.new(2, 1).precision(self.precision) * TrimRat.new(1, 1).precision(self.precision).pi;
		return $tau;
	}
	multi sub term:<tau>() returns TrimRat is export { return TrimRat.new(1, 1).tau; }
	multi sub term:<τ>() returns TrimRat is export { return TrimRat.new(1, 1).tau; }

	#sub mod( FatRat $dividend, FatRat $divisor ) returns FatRat is export {
	#	# Raymond T. Boute's Euclidean definition of the modulo operation.
	#	my FatRat $abs = $divisor.abs;
	#	my FatRat $remainder = $dividend - $abs * FatRat.new( ($dividend / $abs).floor, 1 );
	#	return $remainder;
	#}
	#sub div( FatRat $dividend, FatRat $divisor ) returns FatRat is export {
	#	# Raymond T. Boute's Euclidean definition of the division operation.
	#	my FatRat $quotient = FatRat.new( $divisor.sign * ($dividend / $divisor.abs).floor, 1);
	#	return $quotient;
	#}

	multi sub infix:« + »( TrimRat $a, TrimRat $b ) returns TrimRat is export {
		my FatRat $sum = FatRat.new($a.numerator, $a.denominator) + FatRat.new($b.numerator, $b.denominator);
		my TrimRat $c = TrimRat.new($sum.numerator, $sum.denominator).precision( ( $a.precision, $b.precision ).min );
		return $c;
	}

	multi sub infix:« <=> »( TrimRat $first, TrimRat $second ) returns Int {
		my FatRat $delta = $first.FatRat - $second.FatRat;
		my Rat $proxy = ($delta.floor.Int + $delta.ceiling.Int) / 2;
		if $proxy < 0 { return -1; }
		elsif $proxy > 0 { return 1; }
		else { return 0; }
	}
	multi sub infix:« == »( TrimRat $first, TrimRat $second ) returns Bool {
		return ($first <=> $second) == 0;
	}
	multi sub infix:« != »( TrimRat $first, TrimRat $second ) returns Bool {
		return ($first <=> $second) != 0;
	}
	multi sub infix:« ≠ »( TrimRat $first, TrimRat $second ) returns Bool {
		return ($first <=> $second) != 0;
	}
	multi sub infix:« ≅ »( TrimRat $first, TrimRat $second, TrimRat :$tolerance = TrimRat.new( 1, 10**( ($first.precision, $second.precision).min ) ) ) returns Bool {
		if ($first != 0) and ($second != 0) and ($tolerance != 0) { return ($first - $second).abs < ($first.abs, $second.abs).max * $tolerance; }
		else { return ($first - $second).abs < $tolerance; }
	}
	multi sub infix:« < »( TrimRat $first, TrimRat $second ) returns Bool {
		return ($first <=> $second) == -1;
	}
	multi sub infix:« > »( TrimRat $first, TrimRat $second ) returns Bool {
		return ($first <=> $second) == 1;
	}
	multi sub infix:« <= »( TrimRat $first, TrimRat $second ) returns Bool {
		my Int $compare = $first <=> $second;
		return ($$compare == -1) or ($compare == 0);
	}
	multi sub infix:« ≤ »( TrimRat $first, TrimRat $second ) returns Bool {
		my Int $compare = $first <=> $second;
		return ($$compare == -1) or ($compare == 0);
	}
	multi sub infix:« >= »( TrimRat $first, TrimRat $second ) returns Bool {
		my Int $compare = $first <=> $second;
		return ($$compare == 1) or ($compare == 0);
	}
	multi sub infix:« ≥ »( TrimRat $first, TrimRat $second ) returns Bool {
		my Int $compare = $first <=> $second;
		return ($$compare == 1) or ($compare == 0);
	}

	multi sub infix:« <=> »( TrimRat $first, Int $second ) returns Int {
		return $first <=> $second.TrimRat;
	}
	multi sub infix:« == »( TrimRat $first, Int $second ) returns Bool {
		return $first == $second.TrimRat;
	}
	multi sub infix:« != »( TrimRat $first, Int $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« ≠ »( TrimRat $first, Int $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« ≅ »( TrimRat $first, Int $second ) returns Bool {
		return $first ≅ $second.TrimRat;
	}
	multi sub infix:« < »( TrimRat $first, Int $second ) returns Bool {
		return $first < $second.TrimRat;
	}
	multi sub infix:« > »( TrimRat $first, Int $second ) returns Bool {
		return $first > $second.TrimRat;
	}
	multi sub infix:« <= »( TrimRat $first, Int $second ) returns Bool {
		return $first <= $second.TrimRat;
	}
	multi sub infix:« ≤ »( TrimRat $first, Int $second ) returns Bool {
		return $first <= $second.TrimRat;
	}
	multi sub infix:« >= »( TrimRat $first, Int $second ) returns Bool {
		return $first >= $second.TrimRat;
	}
	multi sub infix:« ≥ »( TrimRat $first, Int $second ) returns Bool {
		return $first >= $second.TrimRat;
	}

	multi sub infix:« <=> »( Int $first, TrimRat $second ) returns Int {
		return $first.TrimRat <=> $second;
	}
	multi sub infix:« == »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat == $second;
	}
	multi sub infix:« != »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« ≠ »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« ≅ »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat ≅ $second;
	}
	multi sub infix:« < »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat < $second;
	}
	multi sub infix:« > »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat > $second;
	}
	multi sub infix:« <= »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat <= $second;
	}
	multi sub infix:« ≤ »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat <= $second;
	}
	multi sub infix:« >= »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat >= $second;
	}
	multi sub infix:« ≥ »( Int $first, TrimRat $second ) returns Bool {
		return $first.TrimRat >= $second;
	}

	multi sub infix:« <=> »( TrimRat $first, Rat $second ) returns Int {
		return $first <=> $second.TrimRat;
	}
	multi sub infix:« == »( TrimRat $first, Rat $second ) returns Bool {
		return $first == $second.TrimRat;
	}
	multi sub infix:« != »( TrimRat $first, Rat $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« ≠ »( TrimRat $first, Rat $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« ≅ »( TrimRat $first, Rat $second ) returns Bool {
		return $first ≅ $second.TrimRat;
	}
	multi sub infix:« < »( TrimRat $first, Rat $second ) returns Bool {
		return $first < $second.TrimRat;
	}
	multi sub infix:« > »( TrimRat $first, Rat $second ) returns Bool {
		return $first > $second.TrimRat;
	}
	multi sub infix:« <= »( TrimRat $first, Rat $second ) returns Bool {
		return $first <= $second.TrimRat;
	}
	multi sub infix:« ≤ »( TrimRat $first, Rat $second ) returns Bool {
		return $first <= $second.TrimRat;
	}
	multi sub infix:« >= »( TrimRat $first, Rat $second ) returns Bool {
		return $first >= $second.TrimRat;
	}
	multi sub infix:« ≥ »( TrimRat $first, Rat $second ) returns Bool {
		return $first >= $second.TrimRat;
	}

	multi sub infix:« <=> »( Rat $first, TrimRat $second ) returns Int {
		return $first.TrimRat <=> $second;
	}
	multi sub infix:« == »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat == $second;
	}
	multi sub infix:« != »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« ≠ »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« ≅ »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat ≅ $second;
	}
	multi sub infix:« < »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat < $second;
	}
	multi sub infix:« > »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat > $second;
	}
	multi sub infix:« <= »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat <= $second;
	}
	multi sub infix:« ≤ »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat <= $second;
	}
	multi sub infix:« >= »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat >= $second;
	}
	multi sub infix:« ≥ »( Rat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat >= $second;
	}

	multi sub infix:« <=> »( TrimRat $first, FatRat $second ) returns Int {
		return $first <=> $second.TrimRat;
	}
	multi sub infix:« == »( TrimRat $first, FatRat $second ) returns Bool {
		return $first == $second.TrimRat;
	}
	multi sub infix:« != »( TrimRat $first, FatRat $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« ≠ »( TrimRat $first, FatRat $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« ≅ »( TrimRat $first, FatRat $second ) returns Bool {
		return $first ≅ $second.TrimRat;
	}
	multi sub infix:« < »( TrimRat $first, FatRat $second ) returns Bool {
		return $first < $second.TrimRat;
	}
	multi sub infix:« > »( TrimRat $first, FatRat $second ) returns Bool {
		return $first > $second.TrimRat;
	}
	multi sub infix:« <= »( TrimRat $first, FatRat $second ) returns Bool {
		return $first <= $second.TrimRat;
	}
	multi sub infix:« ≤ »( TrimRat $first, FatRat $second ) returns Bool {
		return $first <= $second.TrimRat;
	}
	multi sub infix:« >= »( TrimRat $first, FatRat $second ) returns Bool {
		return $first >= $second.TrimRat;
	}
	multi sub infix:« ≥ »( TrimRat $first, FatRat $second ) returns Bool {
		return $first >= $second.TrimRat;
	}

	multi sub infix:« <=> »( FatRat $first, TrimRat $second ) returns Int {
		return $first.TrimRat <=> $second;
	}
	multi sub infix:« == »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat == $second;
	}
	multi sub infix:« != »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« ≅ »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« ≠ »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« < »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat < $second;
	}
	multi sub infix:« > »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat > $second;
	}
	multi sub infix:« <= »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat <= $second;
	}
	multi sub infix:« ≤ »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat <= $second;
	}
	multi sub infix:« >= »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat >= $second;
	}
	multi sub infix:« ≥ »( FatRat $first, TrimRat $second ) returns Bool {
		return $first.TrimRat >= $second;
	}

	multi sub infix:« <=> »( TrimRat $first, Num $second ) returns Int {
		return $first <=> $second.TrimRat;
	}
	multi sub infix:« == »( TrimRat $first, Num $second ) returns Bool {
		return $first == $second.TrimRat;
	}
	multi sub infix:« != »( TrimRat $first, Num $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« ≠ »( TrimRat $first, Num $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« ≅ »( TrimRat $first, Num $second ) returns Bool {
		return $first != $second.TrimRat;
	}
	multi sub infix:« < »( TrimRat $first, Num $second ) returns Bool {
		return $first < $second.TrimRat;
	}
	multi sub infix:« > »( TrimRat $first, Num $second ) returns Bool {
		return $first > $second.TrimRat;
	}
	multi sub infix:« <= »( TrimRat $first, Num $second ) returns Bool {
		return $first <= $second.TrimRat;
	}
	multi sub infix:« ≤ »( TrimRat $first, Num $second ) returns Bool {
		return $first <= $second.TrimRat;
	}
	multi sub infix:« >= »( TrimRat $first, Num $second ) returns Bool {
		return $first >= $second.TrimRat;
	}
	multi sub infix:« ≥ »( TrimRat $first, Num $second ) returns Bool {
		return $first >= $second.TrimRat;
	}

	multi sub infix:« <=> »( Num $first, TrimRat $second ) returns Int {
		return $first.TrimRat <=> $second;
	}
	multi sub infix:« == »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat == $second;
	}
	multi sub infix:« != »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« ≠ »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« ≅ »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat != $second;
	}
	multi sub infix:« < »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat < $second;
	}
	multi sub infix:« > »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat > $second;
	}
	multi sub infix:« <= »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat <= $second;
	}
	multi sub infix:« ≤ »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat <= $second;
	}
	multi sub infix:« >= »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat >= $second;
	}
	multi sub infix:« ≥ »( Num $first, TrimRat $second ) returns Bool {
		return $first.TrimRat >= $second;
	}

};

augment class Int {
	method TrimRat() returns TrimRat { return TrimRat.new(self.Int, 1); }
};
augment class Rat {
	method TrimRat() returns TrimRat { return TrimRat.new($!numerator, $!denominator).norm; }
};
augment class FatRat {
	method TrimRat() returns TrimRat { return TrimRat.new($!numerator, $!denominator).precision( ($!denominator.chars + 1, $DEFAULT-PRECISION).max.abs.UInt ); }
};
augment class Num {
	method TrimRat() returns TrimRat { return TrimRat.new( (self * $DEFAULT-EPSILON.denominator).Int, $DEFAULT-EPSILON.denominator ).norm; }
};

=finish
